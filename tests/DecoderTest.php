<?php

use Paulsnar\Bencode\Decoder;
use Paulsnar\Bencode\DecodingException;

class DecoderTest extends \PHPUnit\Framework\TestCase
{
  protected $decoder;

  public function setUp()
  {
    $this->decoder = new Decoder();
  }

  public function decodableProvider()
  {
    return [
      [ 'i0e', 0 ],
      [ 'i1e', 1 ],
      [ 'i-1e', -1 ],
      [ 'i' . PHP_INT_MAX . 'e', PHP_INT_MAX ],
      [ 'i' . ~PHP_INT_MAX . 'e', ~PHP_INT_MAX ],
      [ '0:', '' ],
      [ '4:test', 'test' ],
      [ '13:a test string', 'a test string' ],
      [ 'li1ei2ei3ei4ee', [ 1, 2, 3, 4 ] ],
      [ 'li1e1:2e', [ 1, '2' ] ],
      [ 'll4:testee', [ [ 'test' ] ] ],
      [ 'd5:firsti1e6:secondi2ee', [ 'first' => 1, 'second' => 2 ] ],
      [ 'd1:dd1:ai1ee1:lli1ei2ee1:s1:s1:ii9ee',
        [ 'd' => [ 'a' => 1 ], 'l' => [ 1, 2 ], 's' => 's', 'i' => 9 ] ],

      // unbalanced inputs
      [ 'i2', null ],
      [ 'l', null ],
      [ 'd', null ],

      // invalid inputs
      [ 'ihe', null ], // invalid digit
      [ '-1:what', null ], // invalid string length prefix
      [ 'di1ei1ee', null ], // invalid dict key
      [ '3:abcde', null ], // trailing string chars
      [ 'ie', null ], // empty integer
    ];
  }

  /**
   * @dataProvider decodableProvider
   */
  public function testDecoding($input, $expected)
  {
    if ($expected === null) {
      $this->expectException(DecodingException::class);
      $this->decoder->decode($input);
    } else {
      $this->assertEquals($expected, $this->decoder->decode($input));
    }
  }
}
