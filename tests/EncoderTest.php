<?php

use Paulsnar\Bencode\Encoder;
use Paulsnar\Bencode\EncodingException;

class EncoderTest extends \PHPUnit\Framework\TestCase
{
  protected $encoder;

  public function setUp()
  {
    $this->encoder = new Encoder();
  }

  public function primitiveProvider()
  {
    return [
      [ true, null ],
      [ false, null ],
      [ 150, 'i150e' ],
      [ -150, 'i-150e' ],
      [ 1.9, null ],
      [ -1.9, null ],
      [ 'string', '6:string' ],
      [ "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f",
        "16:\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f" ],
      [ '', '0:' ],
      [ [ 1, 'second', 3, 'fourth' ],
        'li1e6:secondi3e6:fourthe' ],
      [ [ 'one' => 1, 'two' => 'second' ],
        'd3:onei1e3:two6:seconde' ],
      [ [ 'dict' => [ 'a' => 1, 'b' => 'b' ],
          'int' => 150, 'int2' => -150,
          'list' => [ 1, 'two', 3, 'four' ],
          'string' => 'string' ],
        'd4:dictd1:ai1e1:b1:be3:inti150e4:int2i-150e4:listli1e3:twoi3e4:foure' .
          '6:string6:stringe' ],
      [ [ 'e' => 5, 'a' => 1, 'f' => 6, 'c' => 3, 'b' => 2, 'd' => 4 ],
        'd1:ai1e1:bi2e1:ci3e1:di4e1:ei5e1:fi6ee' ],
    ];
  }

  /**
   * @dataProvider primitiveProvider
   */
  public function testPrimitiveEncoding($encodable, $result)
  {
    if ($result === null) {
      $this->expectException(EncodingException::class);
      $this->encoder->encode($encodable);
    } else {
      $this->assertEquals($result, $this->encoder->encode($encodable));
    }
  }
}
