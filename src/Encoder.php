<?php

namespace Paulsnar\Bencode;

class Encoder
{
  public function encode($item)
  {
    try {
      return $this->encodeTerm($item);
    } catch (\Exception $e) {
      throw new EncodingException(
        'Encoding error', EncodingException::ENCODING_ERROR, $e);
    }
  }

  private function encodeTerm($term)
  {
    switch (gettype($term)) {
    case 'string':
      return mb_strlen($term, '8bit') . ':' .
      mb_convert_encoding($term, '8bit');

    case 'integer':
      return 'i' . $term . 'e';

    case 'array':
      $keys = array_keys($term);
      $ints_only = true;
      foreach ($keys as $key) {
        if (!is_numeric($key)) {
          $ints_only = false;
          break;
        }
      }
      return $ints_only ? $this->encodeList($term) : $this->encodeDict($term);

    default:
      throw new EncodingException(
        sprintf('Cannot encode type: %s', gettype($term)),
        EncodingException::UNENCODABLE_VALUE
      );
    }
  }

  private function encodeList($list)
  {
    $l = 'l';
    $keys = array_keys($list);
    $keys = array_map(function ($k) { return intval($k); }, $keys);
    sort($keys, SORT_STRING);
    foreach ($keys as $key) {
      $l .= $this->encodeTerm($list[$key]);
    }
    $l .= 'e';
    return $l;
  }

  private function encodeDict($dict)
  {
    $d = 'd';
    $keys = array_keys($dict);
    $keys = array_map(function ($k) { return strval($k); }, $keys);
    sort($keys);
    foreach ($keys as $key) {
      $d .= $this->encodeTerm($key);
      $d .= $this->encodeTerm($dict[$key]);
    }
    $d .= 'e';
    return $d;
  }
}
