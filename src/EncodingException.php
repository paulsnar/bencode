<?php

namespace Paulsnar\Bencode;

class EncodingException extends \Exception
{
  const ENCODING_ERROR    = 1;
  const UNENCODABLE_VALUE = 2;
}
