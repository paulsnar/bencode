<?php

namespace Paulsnar\Bencode;

class DecodingException extends \Exception
{
  const DECODING_ERROR    = 1;
  const UNBALANCED_INPUT  = 2;
  const INVALID_DIGIT     = 3;
  const INVALID_TYPE      = 4;
  const MALFORMED_STRING  = 5;
  const MALFORMED_INTEGER = 6;
  const MALFORMED_LIST    = 7;
  const MALFORMED_DICT    = 8;
}
