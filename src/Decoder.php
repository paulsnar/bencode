<?php

namespace Paulsnar\Bencode;

class Decoder
{
  const DIGITS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
  const NUMERIC = ['-', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

  public function decode($string, $safe = false)
  {
    return $this->parseStructure(str_split($string))[0];
  }

  protected function parseStructure($chars, $skipRest = false)
  {
    $type = $chars[0];

    if (in_array($type, self::DIGITS)) {
      return $this->parseString($chars, $skipRest);
    } else if ($type === 'i') {
      return $this->parseInt($chars, $skipRest);
    } else if ($type === 'l') {
      return $this->parseList($chars, $skipRest);
    } else if ($type === 'd') {
      return $this->parseDict($chars, $skipRest);
    } else {
      throw new DecodingException(
        sprintf('Unknown data type: %s', $type),
        DecodingException::INVALID_TYPE);
    }
  }

  protected function parseBareInt($chars, $safe = false)
  {
    if (!$safe) {
      foreach ($chars as $char) {
        if (!in_array($char, self::NUMERIC)) {
          throw new DecodingException(
            sprintf('Invalid digit: "%s"', $char),
            DecodingException::INVALID_DIGIT);
        }
      }
    }

    return [intval(implode($chars)), count($chars)];
  }

  protected function parseInt($chars, $skipRest = false)
  {
    $type = array_shift($chars);
    if ($type !== 'i') {
      throw new DecodingException(
        sprintf('Expected integer, got type: "%s"', $type),
        DecodingException::MALFORMED_INTEGER);
    }
    $number = [ ];
    while (true) {
      if (count($chars) === 0) {
        throw new DecodingException('Unfinished number',
          DecodingException::MALFORMED_INTEGER);
      } else if ($chars[0] === 'e') {
        array_shift($chars);
        break;
      } if (in_array($chars[0], self::NUMERIC)) {
        $number[] = array_shift($chars);
      } else {
        throw new DecodingException(
            sprintf('Invalid integer: expected digit, got: "%s"',
              $chars[0]),
          DecodingException::INVALID_DIGIT);
      }
    }

    if (count($number) === 0) {
      throw new DecodingException('Empty integer',
        DecodingException::MALFORMED_INTEGER);
    }

    if (!$skipRest) {
      if (count($chars) > 0) {
        throw new DecodingException(
            sprintf('Malformed integer: %d leftover chars "%s"',
              count($chars), implode($chars)),
            DecodingException::MALFORMED_INTEGER);
      }
    }

    list($value, $count) = $this->parseBareInt($number, true);
    return [$value, $count + 2];
  }

  protected function parseString($chars, $skipRest = false)
  {
    $length = [ ];
    foreach ($chars as $char) {
      if ($char === ':') {
        break;
      } else if (in_array($char, self::DIGITS)) {
        $length[] = $char;
      } else {
        throw new DecodingException(
            sprintf('Malformed string length prefix: expected digit, got "%s"',
              $char),
          DecodingException::MALFORMED_STRING);
      }
    }
    list($length, $length_length) = $this->parseBareInt($length, true);

    if ($skipRest) {
      $string = array_slice($chars, $length_length + 1, $length);
    } else {
      $string = array_slice($chars, $length_length + 1);
    }
    if (count($string) !== $length) {
      throw new DecodingException(
        sprintf('String length mismatch: expected %d, got %d',
        $length, count($string)),
        DecodingException::MALFORMED_STRING);
      }

    return [implode($string), $length + $length_length + 1];
  }

  protected function parseList($chars, $skipRest = false)
  {
    $type = array_shift($chars);
    if ($type !== 'l') {
      throw new DecodingException(
        sprintf('Expected list, got type: "%s"', $type),
        DecodingException::MALFORMED_LIST);
    }

    $items = [ ];
    $length = 2;

    while (true) {
      if (count($chars) === 0) {
        throw new DecodingException('Unbalanced list',
          DecodingException::UNBALANCED_INPUT);
      } else if ($chars[0] === 'e') {
        array_shift($chars);
        break;
      } else {
        list($term, $term_length) = $this->parseStructure($chars, true);
        $items[] = $term;
        $chars = array_slice($chars, $term_length);
        $length += $term_length;
      }
    }

    if (!$skipRest) {
      if (count($chars) !== 0) {
        throw new DecodingException(
          sprintf('List had trailing characters: "%s"', implode($chars)),
          DecodingException::MALFORMED_LIST);
      }
    }

    return [$items, $length];
  }

  protected function parseDict($chars, $skipRest = false)
  {
    $type = array_shift($chars);
    if ($type !== 'd') {
      throw new DecodingException(
        sprintf('Expected dict, got type: "%s"', $type),
        DecodingException::MALFORMED_DICT);
    }

    $dict = [ ];
    $length = 2;
    $term_i = 0;
    $last_key = '';

    while (true) {
      if (count($chars) === 0) {
        throw new DecodingException('Unbalanced dict',
          DecodingException::UNBALANCED_INPUT);
      } else if ($chars[0] === 'e') {
        array_shift($chars);
        break;
      } else if ($term_i % 2 === 0) {
        list($key, $key_length) = $this->parseString($chars, true);
        $last_key = $key;
        $chars = array_slice($chars, $key_length);
        $length += $key_length;
      } else {
        list($term, $term_length) = $this->parseStructure($chars, true);
        $dict[$last_key] = $term;
        $chars = array_slice($chars, $term_length);
        $length += $term_length;
      }
      $term_i += 1;
    }

    if (!$skipRest) {
      if (count($chars) !== 0) {
        throw new DecodingException(
          sprintf('Dict had trailing characters: "%s"', implode($chars)),
          DecodingException::MALFORMED_DICT);
      }
    }

    return [$dict, $length];
  }
}
